public class Org_TestData {

    public static void setupBusinessRecords() {
        
        List<Business__c> busList = new List<Business__c>();

        busList.add(new Business__c(Name = 'Bus1', Sales_Value__c = 100, Operating_Region__c = 'Delhi'));
        busList.add(new Business__c(Name = 'Bus2', Sales_Value__c = 100, Operating_Region__c = 'Delhi'));
        busList.add(new Business__c(Name = 'Bus3', Sales_Value__c = 100, Operating_Region__c = 'Delhi'));
        busList.add(new Business__c(Name = 'Bus4', Sales_Value__c = 100, Operating_Region__c = 'Mumbai'));
        busList.add(new Business__c(Name = 'Bus5', Sales_Value__c = 50, Operating_Region__c = 'Mumbai'));
        busList.add(new Business__c(Name = 'Bus6', Sales_Value__c = 300, Operating_Region__c = 'Delhi;Mumbai'));
        busList.add(new Business__c(Name = 'Bus7', Sales_Value__c = 100, Operating_Region__c = 'Hyderabad;Mumbai'));
        busList.add(new Business__c(Name = 'Bus8', Sales_Value__c = 200, Operating_Region__c = 'Delhi;Bangalore;Mumbai'));
        
        insert busList;
    }
    
    public static void setupLeadNOppRecords(Integer count) {
        
        Account acc = new Account(Name = 'Testing');
        insert acc;
        
        List<Opportunity> oppties = new List<Opportunity>();
        for(Integer index = 1; index <= count; index++) {
            Opportunity opp = new Opportunity();
            opp.Name = 'Testing Opportunity ' + index;
            opp.Amount = 100;
            opp.StageName = 'Closed Won';
            opp.accountId = acc.Id;
            opp.CloseDate = System.today().addDays(10);
            oppties.add(opp);
        }
        
        insert oppties;
        
        List<Lead> leads = new List<Lead>();
        for(Integer index = 1; index <= count; index++) {
            Lead ld = new Lead();
            ld.FirstName = 'Testing Lead ';
            ld.LastName = String.valueOf(index);
            ld.Company = 'Personal';
            leads.add(ld);
        }
        
        insert leads;
        
    }
}