@isTest
public class SalesDashboardController_Test {
    
    @isTest
    static void test_getCityOptions() {

        Test.startTest();
        Map<String, String> cities = SalesDashboardController.getCityOptions();
        Test.stopTest();

        System.assert(!cities.isEmpty());
    }
}
