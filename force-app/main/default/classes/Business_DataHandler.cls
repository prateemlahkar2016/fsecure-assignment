/* This class will handle the data processing of Business__c */

public class Business_DataHandler {

    
    public static List<Business__c> getBusinessRecords(Set<String> cities) {
        
        String fields = 'Id, Name, Sales_Value__c, Operating_Region__c';
        String query = 'SELECT ' + fields + ' FROM Business__c';
        
        if(cities == null || cities.isEmpty()) return Database.query(query);
        
        String includedCitiesStr = '', excludedCitiesStr = '', whereClause;
        for(String city : getCityOptions().keySet()) {
            if(cities.contains(city)) includedCitiesStr += '\'' + city + '\',';
            else excludedCitiesStr += '\'' + city + '\',';
        }
        
        if(String.isNotBlank(includedCitiesStr)) includedCitiesStr = includedCitiesStr.removeEnd(',');
        if(String.isNotBlank(excludedCitiesStr)) excludedCitiesStr = excludedCitiesStr.removeEnd(',');
        whereClause = ' WHERE Operating_Region__c INCLUDES (' + includedCitiesStr + ') AND Operating_Region__c EXCLUDES('+ excludedCitiesStr +')';
        
        query += whereClause;
        
        return Database.query(query);
    }
    
    public static Map<String, String> getCityOptions() {
        
        Map<String, String> pickListOptions = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = Business__c.Operating_Region__c.getDescribe();
        
        for(Schema.PicklistEntry pEntry : fieldResult.getPicklistValues()) {
            pickListOptions.put(pEntry.getValue(), pEntry.getLabel());
        }
        
        return pickListOptions;
    }
    
}