public class SalesDashboardController {
    
    @auraEnabled
    public static Map<String,String> getCityOptions() {
        return Business_DataHandler.getCityOptions();
    }
}
