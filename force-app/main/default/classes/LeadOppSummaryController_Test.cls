@isTest
public class LeadOppSummaryController_Test {

    @testSetup
    static void setupTestData() {
        Org_TestData.setupLeadNOppRecords(5);
    }
    
    @isTest
    static void test_generateReport() {
        
        Test.startTest();
        List<LeadOppSummaryController.LeadOppSummaryRecord> result = LeadOppSummaryController.generateReport();
        Test.stopTest();
        
        LeadOppSummaryController.LeadOppSummaryRecord ourRecord;
        for(LeadOppSummaryController.LeadOppSummaryRecord record : result) {
            if(record.OwnerId == UserInfo.getUserId()) {
                ourRecord = record;
                break;
            } 
        }
        
        if(ourRecord == null) System.assert(false);
        else {
            System.assertEquals(5, ourRecord.totalLeads);
            System.assertEquals(5, ourRecord.totalOpps);
            System.assertEquals(500, ourRecord.totalOppsValue);
        }
    }
}