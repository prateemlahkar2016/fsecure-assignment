public class SalesReportController {

    public class SalesReportRecord {
        
        @auraEnabled public RegionDetails regionDetails;
        @auraEnabled public String businessName;
        @auraEnabled public Decimal salesValue;
        @auraEnabled public String operatingRegion;
        
        public SalesReportRecord(RegionDetails regionDetails, String businessName, 
                                    Decimal salesValue, String operatingRegion) {
            this.regionDetails = regionDetails;
            this.businessName = businessName;
            this.salesValue = salesValue;
            this.operatingRegion = operatingRegion;
        }
        
    }

    public class RegionDetails {
        @auraEnabled public String regionName {get; private set;}
        @auraEnabled public Decimal totalSales {get; private set;}
        @auraEnabled public Integer noOfBusinesses {get; private set;}

        private RegionDetails(String regionName, Decimal totalSales, Integer noOfBusinesses) {
            this.regionName = regionName;
            this.totalSales = totalSales;
            this.noOfBusinesses = noOfBusinesses;
        }
    }
    
    @auraEnabled
    public static List<SalesReportRecord> getSalesReport(String city1, String city2) {
        
        Set<String> cities = new Set<String>();
        if(String.isNotBlank(city1)) cities.add(city1);
        if(String.isNotBlank(city2)) cities.add(city2);
        
        List<Business__c> busList = Business_DataHandler.getBusinessRecords(cities);
        
        if(busList <> null && !busList.isEmpty()) return getSalesReportRecords(busList);
        return null;
    }
       
    private static List<SalesReportRecord> getSalesReportRecords(List<Business__c> busList) {
        
        List<SalesReportRecord> records = new List<SalesReportRecord>();
        Map<String, RegionDetails> regionDetailsMap = new Map<String, RegionDetails>();

        for(Business__c bus : busList) {

            SalesReportRecord record;
            RegionDetails rDetails;
            
            if(!regionDetailsMap.keyset().contains(bus.Operating_Region__c)) {
                rDetails = new RegionDetails(replaceSColonWithComma(bus.Operating_Region__c), bus.Sales_Value__c, 1);
                regionDetailsMap.put(bus.Operating_Region__c, rDetails);
                record = new SalesReportRecord(rDetails, bus.Name, 
                                                    bus.Sales_Value__c, replaceSColonWithComma(bus.Operating_Region__c));
            }
            else {
                rDetails = regionDetailsMap.get(bus.Operating_Region__c);
                rDetails.totalSales += bus.Sales_Value__c;
                rDetails.noOfBusinesses++;
                regionDetailsMap.put(bus.Operating_Region__c, rDetails);
                record = new SalesReportRecord(null, bus.Name, 
                                                    bus.Sales_Value__c, replaceSColonWithComma(bus.Operating_Region__c));
            }

            records.add(record);
        }

        return records;
    }

    private static String replaceSColonWithComma(String operatingName) {
        return operatingName.replace(';',',');
    }
}