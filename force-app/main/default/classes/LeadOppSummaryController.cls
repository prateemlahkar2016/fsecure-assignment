public class LeadOppSummaryController {

    private static final Integer MAX_ROWS = 50000;
    private static Map<Id, LeadOppSummaryRecord> ownerIdSummaryMap;

    public class LeadOppSummaryRecord {
        @auraEnabled public String ownerId;
        @auraEnabled public String ownerName;
        @auraEnabled public String ownerCurrencyISOCode;
        @auraEnabled public Integer totalLeads;
        @auraEnabled public Integer totalOpps;
        @auraEnabled public Date latestOppCreatedDate;
        @auraEnabled public Decimal totalOppsValue;
        
        public LeadOppSummaryRecord(String ownerId, String ownerName, String ownerCurrISOCode, Integer totalLeads, 
                        Integer totalOpps, Date latestOppCreatedDate, Decimal totalOppsValue) {
            this.ownerId = ownerId;
            this.ownerName = ownerName;
            this.ownerCurrencyISOCode = ownerCurrISOCode;
            this.totalLeads = totalLeads;
            this.totalOpps = totalOpps;
            this.latestOppCreatedDate = latestOppCreatedDate;
            this.totalOppsValue = totalOppsValue;         
        }
    }
    
    @auraEnabled 
    public static List<LeadOppSummaryRecord> generateReport() {
        
        List<User> users = new List<User>();
        Set<Id> ownerIds = new Set<Id>();
        ownerIdSummaryMap = new Map<Id, LeadOppSummaryRecord>();
        
        for(User u : [SELECT Id, Name, CurrencyISOCode FROM User WHERE UserType = 'Standard' LIMIT :MAX_ROWS]) {
            users.add(u);
            ownerIdSummaryMap.put(u.Id, new LeadOppSummaryRecord(u.Id, u.Name, u.CurrencyISOCode, 0, 0, null, 0.0));
        }
        
        getLeadSummary();
        getOppSummary();
        
        return ownerIdSummaryMap.values();
    }
    
    private static void getLeadSummary() {
        for(AggregateResult aResult : [SELECT COUNT(Name) TotalLeads, ownerId 
                                            FROM Lead 
                                            WHERE OwnerId IN :ownerIdSummaryMap.keySet() 
                                                GROUP BY OwnerId]) {
            Id ownerId = (Id) aResult.get('OwnerId');
            LeadOppSummaryRecord record = ownerIdSummaryMap.get(ownerId);
            record.totalLeads = Integer.valueOf(aResult.get('TotalLeads'));
            ownerIdSummaryMap.put(ownerId, record);
        }
    }
    
    private static void getOppSummary() {
        for(AggregateResult aResult : [SELECT COUNT(Name) TotalOpps, MAX(CreatedDate) LatestCreatedDate, 
                                                SUM(Amount) TotalAmount, OwnerId
                                            FROM Opportunity
                                            WHERE OwnerId IN :ownerIdSummaryMap.keySet()
                                                GROUP BY OwnerId]) {
            
            Id ownerId = (Id) aResult.get('OwnerId');
            LeadOppSummaryRecord record = ownerIdSummaryMap.get(ownerId);
            record.totalOpps = (Integer.valueOf(aResult.get('TotalOpps')));
            record.latestOppCreatedDate = Date.valueOf(aResult.get('LatestCreatedDate'));
            record.totalOppsValue = (Decimal) aResult.get('TotalAmount');
            ownerIdSummaryMap.put(ownerId, record);
        }
    }
}