@isTest
public class Business_DataHandler_Test {
    
    @testSetup
    static void setupTestData() {
        Org_TestData.setupBusinessRecords();
    }
    
    @isTest
    static void test_getBusinessRecordsWithCities() {
        
        Test.startTest();
        Set<String> cities = new Set<String> {'Delhi', 'Mumbai'};
        List<Business__c> busList = Business_DataHandler.getBusinessRecords(cities);
        Test.stopTest();
        
        System.assertEquals(busList.size(), 6);
        
    }
    
   @isTest
    static void test_getBusinessRecordsWithoutCities() {
        
        Test.startTest();
        Set<String> cities = new Set<String>();
        List<Business__c> busList = Business_DataHandler.getBusinessRecords(cities);
        Test.stopTest();
        
        System.assertEquals(busList.size(), 8);
        
    }
}