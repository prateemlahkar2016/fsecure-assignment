@isTest
public class SalesReportController_Test {

    @testSetup
    static void setupTestData() {
        Org_TestData.setupBusinessRecords();
    }
    
    
    @isTest
    static void test_getSalesReport() {
        
        Test.startTest();
        List<SalesReportController.SalesReportRecord> result = SalesReportController.getSalesReport('Delhi', 'Mumbai');
        Test.stopTest();
        
        System.assertEquals(result.size(), 6);
    }
    
}