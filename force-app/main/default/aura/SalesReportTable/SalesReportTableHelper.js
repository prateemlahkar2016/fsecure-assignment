({
	formReport : function(component, city1, city2) { 
		
        var actionParams = {
            city1 : city1,
            city2 : city2
        };
        
        var actionCallback = function(response) {           
			var state = response.getState();
            if(state === "SUCCESS") {        
                var result = response.getReturnValue();
                component.set('v.recordSet', result);
            }
            else if (state === "ERROR") {
                this.handleError(response.getError());
            }
            
            component.set('v.isLoading', false);
        }
        
        this.callAction(component, 'c.getSalesReport', actionParams, actionCallback);  
    }
})