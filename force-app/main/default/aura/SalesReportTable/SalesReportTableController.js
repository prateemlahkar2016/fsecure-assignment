({
	doInit : function(component, event, helper) {	
        var city1 = component.get('v.city1');
        var city2 = component.get('v.city2');
        
        component.set('v.isLoading', true);
        helper.formReport(component, city1, city2);
	}
})