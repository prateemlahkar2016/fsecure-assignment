({
	getCityOptions : function(component) {
		
        var actionCallback = function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var results = response.getReturnValue();
                var options = this.populateOptions(results, null);
                component.set('v.cities', results);
                component.set('v.city1Options', options);
            }
            else if(state === "ERROR") {
                this.handleError(response.getError());
            }     
        }      
        this.callAction(component, 'c.getCityOptions', null, actionCallback); 
	},
    
    populateOptions : function(responseValues, cityName) {
        var cityOptions = [];
    
        for(var index in responseValues) {
            if(cityName !== null && cityName == index) continue;
            cityOptions.push({ label : responseValues[index], value : index });
        }
        return cityOptions;
    },
    
    reloadReportTable: function(component) {
        var city1 = component.get('v.selectedCity1');
        var city2 = component.get('v.selectedCity2');

        if((city1 !== '' && city2 !== '') || (city1 === '' && city2 === '')) {
            var reportTable = component.find('reportTable');
        	reportTable.reloadTable();
        }  
    }
})