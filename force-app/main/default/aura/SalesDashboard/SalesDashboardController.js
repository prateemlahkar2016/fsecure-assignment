({
	doInit : function(component, event, helper) {
		helper.getCityOptions(component);
	},
    setCity2Options : function(component, event, helper) {
         
        var city1 = component.get('v.selectedCity1');
        component.set('v.selectedCity2', '');
        component.set('v.city2Options', null);
        
        if(city1 !== null && city1 !== '') {
            var cities = component.get('v.cities');
            component.set('v.city2Options', helper.populateOptions(cities, city1));
        }
        
        helper.reloadReportTable(component);
    },
    reloadReportTable : function(component, event, helper) {
        helper.reloadReportTable(component);
    }
})