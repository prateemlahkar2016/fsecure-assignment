({
	formReport : function(component) {
        var actionCallback = function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.recordSet', result);
            }
            else if (state === "ERROR") {
                this.handleError(response.getError());
            }
            
            component.set('v.isLoading', false);
        }
        
        this.callAction(component, 'c.generateReport', null, actionCallback);
	}
})