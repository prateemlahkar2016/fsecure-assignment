# FSecure-Assignment

This is the SFDX repository of the Lightning Application. For more details please view the [wiki](https://bitbucket.org/prateemlahkar2016/fsecure-assignment/wiki/Home)

## Installation

You can install the package using the instructions mentioned in the Wiki. But if you want to download this 
repository and work on it, here are the instructions

To deploy on a Dev hub org

```
$ git clone git@bitbucket.org:prateemlahkar2016/fsecure-assignment.git
$ sfdx force:source:deploy -p ./force-app/main/default -u <devhubusername>
```

To push to a scratch org 

```
$ git clone git@bitbucket.org:prateemlahkar2016/fsecure-assignment.git
$ sfdx force:source:push -u <scratchorg_username>
```

Once you are done, execute the following to assign the permission set to yourself

```
sfdx force:user:permset:assign -n Assignment_User_Access -u <orgname>
```